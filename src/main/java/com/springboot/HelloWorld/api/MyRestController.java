package com.springboot.HelloWorld.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
public class MyRestController {

    @GetMapping("/")
    public String sayHello() {
        return "The time on the server is: " + LocalDateTime.now();
    }
}
